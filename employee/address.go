package employee

func AddressConstruct(dn string, l1 string, l2 string, z string) address {
	return address{
		dno:   dn,
		lane1: l1,
		lane2: l2,
		zip:   z,
	}
}

type address struct {
	dno   string
	lane1 string
	lane2 string
	zip   string
}
