package employee

import (
	"time"
)

func PersonConstruct(n string, d time.Time, a int, ad address) *person {

	pr := person{
		name: n,
		dob:  d,
		age:  a,
		addr: ad,
	}
	return &pr
}

type person struct {
	name string
	dob  time.Time
	age  int
	addr address
}
